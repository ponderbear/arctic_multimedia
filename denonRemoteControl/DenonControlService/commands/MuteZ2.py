import logging
import DataCache as cache
import commands._SimpleCommand as cmdSimple

logger = logging.getLogger(__name__)

_id = 'muteZ2'
_prefix = 'Z2MU'
cachedValue = cache.CachedValue(_id)


def getId():
    return _id


def cmdPrefix():
    return _prefix


def createRequest(request='get'):
    cachedValue.invalidate()
    request = request.upper()

    if request in ('GET'):
        # Special prefix for getting Z2 mute state
        return cmdPrefix()  + '?\r'
    elif request in ('ON', 'OFF'):
        return cmdPrefix() + request + '\r'
    elif request.isdecimal() is True:
        return cmdPrefix() + request[:2] + '\r'

    logger.debug('Ignore unknwon request')
    return None


def isProcessible(reply):
    return cmdSimple.isProcessible(cmdPrefix(), reply)


def processReply(reply):
    return cmdSimple.processReply(cmdPrefix(), reply, cachedValue)
